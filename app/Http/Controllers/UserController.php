<?php

namespace App\Http\Controllers;

use App\Http\Resources\UserResource;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use JWTAuth;
use JWTAuthException;

class UserController extends Controller
{
    protected $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function user()
    {
        return User::whereId(1)->get();
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function authenticate(Request $request)
    {

        $credentials = $request->only('email', 'password');

        $token = null;
        try {
            if (!$token = JWTAuth::attempt($credentials)) {
                return response()->json([
                  'response' => 'error',
                  'message' => 'invalid_email_or_password',
                ], 400);
            }
        } catch (JWTAuthException $e) {
            return response()->json([
              'response' => 'error',
              'message' => 'failed_to_create_token',
            ], 500);
        }

        $user = JWTAuth::user();

        return response()->json(compact('token','user'));
    }

    public function addUser(Request $request)
    {
        $user = User::create($request->all());

        $password = 'secret';
        
        $user->password = $password;
        $user->save();

        return response()->json([
          'user' => $user
        ], 200);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAuthenticatedUser()
    {
        try {
            if (!$user = JWTAuth::parseToken()->authenticate()) {
                return response()->json(['user_not_found'], 404);
            }
        } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
            return response()->json(['token_expired'], $e->getStatusCode());
        } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {
            return response()->json(['token_invalid'], $e->getStatusCode());
        } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {
            return response()->json(['token_absent'], $e->getStatusCode());
        }

        $user = new UserResource($user);

        return response()->json(compact('user'));
    }
}
