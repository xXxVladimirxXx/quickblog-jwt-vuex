require('./bootstrap');

import Vue from 'vue'
import Vuex from 'vuex';
import Vuetify from 'vuetify'
import VueRouter from 'vue-router'
import {routes} from './routes';
import modules from './store';
import {initialize} from './general';

Vue.use(Vuex);
Vue.use(Vuetify)
import 'vuetify/dist/vuetify.min.css'

export const store = new Vuex.Store({
    ...modules
})

Vue.use(VueRouter)

export const router = new VueRouter({
    // mode: 'history',
    routes
});

initialize(store, router)
  
const app = new Vue({
    el: '#app',
    store,
    router
});
