import Main  from '../Main';
import Articles  from '../components/Articles';
import Home  from '../components/Home';
import Admin  from '../components/admin/Admin';
import Login from '../components/auth/Login'
import Register from '../components/auth/Register'

export const routes = [
    {
        path: '/',
        component: Main,
        children: [
            {
                path: '',
                component: Home,
            },
            {
                path: 'articles',
                component: Articles
            },
            {
                path: 'admin',
                component: Admin,
                meta: {
                    requiresAuth: true
                }
            }
        ],
    },
    {
        path: '/login',
        component: Login,
    },
    {
        path: '/register',
        component: Register,
    }
];
