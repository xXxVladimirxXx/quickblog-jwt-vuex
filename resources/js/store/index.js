import articles from './articles'
import users from './users'

export default {
  modules: {
    articles,
    users
  }
}
